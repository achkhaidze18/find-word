package com.voida.service.findword.web.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@ToString
public class VerifyTokenDTO {

    @NotEmpty
    private String appKeyword;
}
