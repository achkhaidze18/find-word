package com.voida.service.findword.web.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class GameDTO {
    private Integer totalQuestions;
    private Integer playedQuestions;
    private Integer remainingQuestions;
    private String expirationDate;
    private String creationDate;
    private Integer points;
}
