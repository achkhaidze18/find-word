package com.voida.service.findword.web.controller;

import com.voida.logging.*;
import com.voida.service.findword.persistence.model.sso.VerifyTokenResponse;
import com.voida.service.findword.service.PrizeService;
import com.voida.service.findword.service.SsoIntegrationService;
import com.voida.service.findword.service.UserScoreManager;
import com.voida.service.findword.web.dto.*;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.voida.service.findword.web.controller.MappingConstants.*;

@RestController
@RequiredArgsConstructor
public class UserScoreController {
    private final UserScoreManager userScoreManager;
    private final SsoIntegrationService ssoIntegrationService;
    private final PrizeService prizeService;

    @Audited(uniqueId = @UniqueId(create = CreateStrategy.IF_NOT_EXISTS, remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.ALL)
    @GetMapping(LEADER_BOARD_URL)
    public LeaderBoardDTO getLeaderBoard(@RequestParam(name = "appKeyword") String appKeyword,
                                         @ApiParam(value = "1-daily, 2-weekly, 3-monthly", example = "1")
                                                @RequestParam(name = "periodId") Integer periodId,
                                         @RequestHeader(value = HEADER_STRING) String token) {
        VerifyTokenResponse verifyTokenResponse = ssoIntegrationService.verifyToken(appKeyword, token);
        return userScoreManager.getLeaderBoard(verifyTokenResponse.getAccount().getId(), appKeyword, periodId);
    }

    @Audited(uniqueId = @UniqueId(create = CreateStrategy.IF_NOT_EXISTS, remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.ALL)
    @GetMapping(STATISTICS_URL)
    public StatisticsDTO getStatistics(@RequestParam(name = "appKeyword") String appKeyword,
                                       @RequestHeader(value = HEADER_STRING) String token) {
        VerifyTokenResponse verifyTokenResponse = ssoIntegrationService.verifyToken(appKeyword, token);
        return userScoreManager.getStatistics(verifyTokenResponse.getAccount().getId());
    }

    @Audited(uniqueId = @UniqueId(create = CreateStrategy.IF_NOT_EXISTS, remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.ALL)
    @GetMapping(HISTORY_URL)
    public HistoryDTO getGameHistory(@RequestParam(name = "appKeyword") String appKeyword,
                                     @RequestParam(name = "page", required = false) Integer page,
                                     @RequestHeader(value = HEADER_STRING) String token) {
        VerifyTokenResponse verifyTokenResponse = ssoIntegrationService.verifyToken(appKeyword, token);
        return userScoreManager.getHistory(verifyTokenResponse.getAccount().getId(), page);
    }

    @Audited(uniqueId = @UniqueId(create = CreateStrategy.IF_NOT_EXISTS, remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.ALL)
    @GetMapping(PRIZE_URL)
    public PrizesDTO getPrizes(@RequestParam(name = "appKeyword") String appKeyword,
                               @ApiParam(value = "1-daily, 2-weekly, 3-monthly", example = "1")
                                    @RequestParam(name = "periodId", required = false) Integer periodId,
                               @RequestHeader(value = HEADER_STRING) String token) {
        ssoIntegrationService.verifyToken(appKeyword, token);
        return prizeService.getAllByPeriodId(periodId);
    }

}
