package com.voida.service.findword.web.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AnswerResultDTO {
    private Boolean isCorrect;
    private Integer points;
    private String correctAnswer;
}
