package com.voida.service.findword.web.controller;

import com.voida.logging.*;
import com.voida.service.findword.service.DataService;
import com.voida.service.findword.web.dto.ApiResponseDTO;
import com.voida.service.findword.web.dto.DataRefreshDTO;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import static com.voida.service.findword.web.controller.MappingConstants.REFRESH_DATA_URL;

@RestController
@RequiredArgsConstructor
@ApiIgnore //TODO: remove and add in swagger locked section
public class DataController {
    private final DataService dataService;

    @PostMapping(REFRESH_DATA_URL)
    @ApiOperation("This method is used to evict caches.")
    @Audited(uniqueId = @UniqueId(create = CreateStrategy.IF_NOT_EXISTS, remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.ALL)
    public ApiResponseDTO refreshData(@RequestBody(required = false) DataRefreshDTO dataRefreshDTO) {
        if(dataRefreshDTO == null || dataRefreshDTO.getValue() == null) return dataService.evictAllCaches();
        return dataService.evictCache(dataRefreshDTO.getValue());
    }
}
