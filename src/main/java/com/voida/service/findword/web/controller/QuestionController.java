package com.voida.service.findword.web.controller;

import com.voida.logging.*;
import com.voida.service.findword.persistence.model.sso.VerifyTokenResponse;
import com.voida.service.findword.service.SsoIntegrationService;
import com.voida.service.findword.service.QuestionManager;
import com.voida.service.findword.web.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.voida.service.findword.web.controller.MappingConstants.*;

@RestController
@RequiredArgsConstructor
public class QuestionController {
    private final QuestionManager questionManager;
    private final SsoIntegrationService ssoIntegrationService;

    @Audited(uniqueId = @UniqueId(create = CreateStrategy.IF_NOT_EXISTS, remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.ALL)
    @GetMapping(QUESTION_URL)
    public QuestionDTO getQuestion(@RequestParam(name = "appKeyword") String appKeyword,
                                   @RequestParam(name = "languageId") Integer languageId,
                                   @RequestHeader(value = HEADER_STRING) String token) {
        VerifyTokenResponse verifyTokenResponse = ssoIntegrationService.verifyToken(appKeyword, token);
        return questionManager.getQuestion(verifyTokenResponse.getAccount().getId(), languageId);
    }

    @Audited(uniqueId = @UniqueId(create = CreateStrategy.IF_NOT_EXISTS, remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.ALL)
    @PostMapping(ANSWER_URL)
    public AnswerResultDTO checkAnswer(@RequestBody @Valid AnswerDTO answerDTO,
                                     @RequestHeader(value = HEADER_STRING) String token) {
        VerifyTokenResponse verifyTokenResponse = ssoIntegrationService.verifyToken(answerDTO.getAppKeyword(), token);
        return questionManager.checkAnswer(verifyTokenResponse.getAccount().getId(), answerDTO.getAnswer(), answerDTO.getSpentTime());
    }

    @Profile({"wordGameDev", "wordGameTest", "wordGameProd"})
    @Audited(uniqueId = @UniqueId(create = CreateStrategy.IF_NOT_EXISTS, remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.ALL)
    @GetMapping(HINT_URL)
    public HintDTO getHint(@RequestParam(name = "appKeyword") String appKeyword,
                           @RequestHeader(value = HEADER_STRING) String token) {
        VerifyTokenResponse verifyTokenResponse = ssoIntegrationService.verifyToken(appKeyword, token);
        return questionManager.getHint(verifyTokenResponse.getAccount().getId());
    }
}
