package com.voida.service.findword.web.controller;

import com.voida.logging.*;
import com.voida.service.findword.service.QuestionGenerationService;
import com.voida.service.findword.web.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import static com.voida.service.findword.web.controller.MappingConstants.*;

@RestController
@RequiredArgsConstructor
@Profile({"mathGameDev", "mathGameTest", "mathGameProd"})
public class MathGameController {
    private final QuestionGenerationService questionGenerationService;

    @Audited(uniqueId = @UniqueId(create = CreateStrategy.IF_NOT_EXISTS, remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.ALL)
    @PostMapping(MATH_QUESTION_GENERATION_URL)
    public ApiResponseDTO generateMathQuestions() {
        return questionGenerationService.generateMathQuestions();
    }

}
