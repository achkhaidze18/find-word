package com.voida.service.findword.web.controller;

import com.voida.logging.*;
import com.voida.service.findword.persistence.model.sso.VerifyTokenResponse;
import com.voida.service.findword.service.SsoIntegrationService;
import com.voida.service.findword.service.SubscriptionService;
import com.voida.service.findword.web.dto.ExtraGameDTO;
import com.voida.service.findword.web.dto.VerifyTokenDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

import static com.voida.service.findword.web.controller.MappingConstants.*;

@RestController
@RequiredArgsConstructor
@ApiIgnore
public class SubscriptionController {
    private final SubscriptionService subscriptionService;
    private final SsoIntegrationService ssoIntegrationService;

    @Audited(uniqueId = @UniqueId(create = CreateStrategy.IF_NOT_EXISTS, remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.ALL)
    @PostMapping(VERIFY_TOKEN_URL)
    public VerifyTokenResponse verifyTokenWithSSO(@RequestHeader(value = HEADER_STRING) String token,
                                           @RequestBody @Valid VerifyTokenDTO verifyTokenDTO) {
        return ssoIntegrationService.verifyToken(verifyTokenDTO.getAppKeyword(), token);
    }

    @Audited(uniqueId = @UniqueId(create = CreateStrategy.IF_NOT_EXISTS, remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.ALL)
    @PostMapping(EXTRA_GAME_URL)
    public ExtraGameDTO getExtraGame(@RequestHeader(value = HEADER_STRING) String token,
                                     @RequestBody @Valid VerifyTokenDTO verifyTokenDTO) {
        VerifyTokenResponse verifyTokenResponse = ssoIntegrationService.verifyToken(verifyTokenDTO.getAppKeyword(), token);
        return subscriptionService.getExtraGame(verifyTokenDTO.getAppKeyword(), verifyTokenResponse.getAccount().getId());
    }

}
