package com.voida.service.findword.web.controller;

import com.voida.logging.Audited;
import com.voida.logging.LoggingPolicy;
import com.voida.logging.RemoveStrategy;
import com.voida.logging.UniqueId;
import com.voida.service.findword.service.exception.QuestionLimitExceededException;
import com.voida.service.findword.service.exception.*;
import com.voida.service.findword.web.dto.ErrorDTO;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Log4j2
public class ControllerExceptionHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @Audited(uniqueId = @UniqueId(remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.OUTPUT)
    @ExceptionHandler(value = {
            QuestionNotFoundException.class,
            LevelNotFoundException.class,
            UserNotFoundException.class,
            UserScoreNotFoundException.class,
            PageInfoNotFoundException.class,
            LanguageNotFoundException.class,
            PeriodNotFoundException.class
    })
    public ResponseEntity<ErrorDTO> handleNotFoundExceptions(RuntimeException e) {
        return returnErrorResponse(HttpStatus.NOT_FOUND, e);
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @Audited(uniqueId = @UniqueId(remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.OUTPUT)
    @ExceptionHandler(value = {
            QuestionLimitExceededException.class
    })
    public ResponseEntity<ErrorDTO> handleForbiddenExceptions(RuntimeException e) {
        return returnErrorResponse(HttpStatus.FORBIDDEN, e);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @Audited(uniqueId = @UniqueId(remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.EXCEPTION)
    @ExceptionHandler(value = { MethodArgumentNotValidException.class })
    public ResponseEntity<ErrorDTO> handleValidationExceptions(MethodArgumentNotValidException e) {
        FieldError fieldError = e.getBindingResult().getFieldError();
        String message = "Argument validation failed. ";
        if(fieldError != null) message += "Invalid " + fieldError.getField() + ".";
        else if(e.getBindingResult().hasErrors()) message += e.getBindingResult().getAllErrors().get(0).getDefaultMessage() + ".";
        ResponseEntity<ErrorDTO> res = returnErrorResponse(HttpStatus.BAD_REQUEST, new RuntimeException(message));
        log.error(res.getBody());
        return res;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @Audited(uniqueId = @UniqueId(remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.OUTPUT)
    @ExceptionHandler(value = {
            UserCreationException.class,
            UserScoreCreationException.class,
            UserQuestionCreationException.class,
            UserGameCreationException.class,

            UserScoreUpdateException.class,

            QuestionMismatchException.class,
            SubscriptionChargeException.class,
            QuestionCreationException.class,
            AnswerEvaluationException.class
    })
    public ResponseEntity<ErrorDTO> handleBadRequestExceptions(Exception e) {
        return returnErrorResponse(HttpStatus.BAD_REQUEST, e);
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = {
            TokenVerificationException.class,
    })
    @Audited(uniqueId = @UniqueId(remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.EXCEPTION)
    public ResponseEntity<ErrorDTO> logCustomExceptions(CustomException e) {
        return returnErrorResponse(HttpStatus.UNAUTHORIZED, e);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @Audited(uniqueId = @UniqueId(remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.OUTPUT)
    @ExceptionHandler(value = {
            Throwable.class,
            UserAccountDataException.class
    })
    public ResponseEntity<ErrorDTO> logServerErrors(Exception e) {
        ResponseEntity<ErrorDTO> res = returnErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e);
        log.error(res.getBody());
        return res;
    }

    private ResponseEntity<ErrorDTO> returnErrorResponse(HttpStatus status, Exception e) {
        ErrorDTO errorDto = ErrorDTO.builder()
                .code(status.value())
                .status(status.getReasonPhrase())
                .message(e.getMessage())
                .build();
        if(e instanceof CustomException) errorDto.setCustomErrorCode(((CustomException) e).getCode());
        return ResponseEntity.status(status).body(errorDto);
    }
}
