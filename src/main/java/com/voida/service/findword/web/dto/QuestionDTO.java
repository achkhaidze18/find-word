package com.voida.service.findword.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class QuestionDTO {
    private String question;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String answer;
    private GameDTO game;
}
