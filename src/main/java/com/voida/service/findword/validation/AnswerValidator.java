package com.voida.service.findword.validation;

import com.voida.service.findword.service.ValidationService;
import lombok.RequiredArgsConstructor;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@RequiredArgsConstructor
public class AnswerValidator implements ConstraintValidator<AnswerConstraint, String> {
    private final ValidationService validationService;

    @Override
    public void initialize(AnswerConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(String answer, ConstraintValidatorContext ctx) {
        return validationService.isValid(answer);
    }

}