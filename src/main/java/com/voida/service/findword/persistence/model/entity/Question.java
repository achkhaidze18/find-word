package com.voida.service.findword.persistence.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "question", columnDefinition = "NVARCHAR(250)", nullable = false)
    private String question;

    @Column(name = "answer")
    private Integer answer;

    @Column(name = "solution", columnDefinition = "NVARCHAR(250)")
    private String solution;

    @Column(name = "hint", columnDefinition = "NVARCHAR(50)")
    private String hint;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "topic_id")
    private Topic topic;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "level_id")
    private Level level;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "language_id")
    private Language language;
}
