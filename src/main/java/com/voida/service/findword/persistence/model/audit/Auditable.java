package com.voida.service.findword.persistence.model.audit;

public interface Auditable {
    Audit getAudit();
    void setAudit(Audit audit);
}
