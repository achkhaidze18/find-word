package com.voida.service.findword.persistence.repository;

import com.voida.service.findword.persistence.model.entity.Period;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeriodRepository extends JpaRepository<Period, Integer> {
}
