package com.voida.service.findword.persistence.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
public class Prize {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "giftKey", columnDefinition = "NVARCHAR(50)")
    private String giftKey;

    @Column(name = "icon", columnDefinition = "NVARCHAR(50)")
    private String icon;

    @Column(name = "price", columnDefinition = "decimal")
    private BigDecimal price;

    @JoinColumn(name = "type", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private PrizeType type;

    @Column(name = "name", columnDefinition = "NVARCHAR(50)")
    private String name;

    @Column(name = "description", columnDefinition = "NVARCHAR(250)")
    private String description;

    @JoinColumn(name = "period", nullable = false)
    @OneToOne(fetch = FetchType.LAZY)
    private Period period;
}
