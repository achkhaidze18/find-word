package com.voida.service.findword.persistence.model.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum PeriodTypes {
    DAILY(1),
    WEEKLY(2),
    MONTHLY(3),
    OVERALL(4);

    private final Integer value;
}
