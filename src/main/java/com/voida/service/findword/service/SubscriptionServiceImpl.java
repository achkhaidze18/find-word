package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.User;
import com.voida.service.findword.persistence.model.entity.UserGame;
import com.voida.service.findword.web.dto.ChargeResponseDTO;
import com.voida.service.findword.web.dto.ExtraGameDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
public class SubscriptionServiceImpl implements SubscriptionService {
    private final UserGameService userGameService;
    private final UserService userService;
    private final SsoIntegrationService ssoIntegrationService;

    @Override
    public ExtraGameDTO getExtraGame(String keyword, Long accountId) {
        User user = userService.createIfNotExists(accountId);
        ChargeResponseDTO chargeResponse = ssoIntegrationService.directCharge(keyword, accountId);
        if(!chargeResponse.getSuccess()) return new ExtraGameDTO(false, chargeResponse.getMessage(), null);
        UserGame userGame = userGameService.create(user);
        return new ExtraGameDTO(true, null, userGameService.getGameDTO(userGame));
    }

}
