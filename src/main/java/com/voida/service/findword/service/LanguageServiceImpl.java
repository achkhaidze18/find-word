package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.Language;
import com.voida.service.findword.persistence.repository.LanguageRepository;
import com.voida.service.findword.service.exception.LanguageNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LanguageServiceImpl implements LanguageService {
    private final LanguageRepository languageRepository;

    @Override
    @Cacheable("default_language")
    public Language getDefault() {
        return languageRepository.findByIsDefaultIsTrue()
                .orElseThrow(() -> new LanguageNotFoundException("Default language was not found."));
    }
}
