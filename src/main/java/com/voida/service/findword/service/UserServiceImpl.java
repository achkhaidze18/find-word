package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.User;
import com.voida.service.findword.persistence.repository.UserRepository;
import com.voida.service.findword.service.exception.UserCreationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Log4j2
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserScoreService userScoreService;

    private User create(Long accountId) {
        User user = new User();
        user.setAccountId(accountId);

        try {
            return userRepository.save(user);
        } catch (Throwable t) {
            throw new UserCreationException(t.getMessage());
        }
    }

    @Override
    @Cacheable("user")
    public User createIfNotExists(Long accountId) {
        User user = userRepository.getByAccountId(accountId);
        if(user == null) {
            user = create(accountId);
            userScoreService.create(user);
        }
        return user;
    }

}