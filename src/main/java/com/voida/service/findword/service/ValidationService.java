package com.voida.service.findword.service;

public interface ValidationService {
    boolean isValid(String answer);
}
