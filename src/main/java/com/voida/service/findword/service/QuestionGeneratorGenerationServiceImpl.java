package com.voida.service.findword.service;

import com.voida.service.findword.config.MathGameLimitProperties;
import com.voida.service.findword.persistence.model.entity.Level;
import com.voida.service.findword.persistence.model.entity.Question;
import com.voida.service.findword.service.exception.QuestionCreationException;
import com.voida.service.findword.web.dto.ApiResponseDTO;
import com.voida.service.findword.web.dto.MathQuestionDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@RequiredArgsConstructor
@Profile({"mathGameDev", "mathGameTest", "mathGameProd"})
public class QuestionGeneratorGenerationServiceImpl implements QuestionGenerationService {
    private final MathGameLimitProperties mathGameLimitProperties;
    private final QuestionDAO questionService;
    private final LevelService levelService;
    private final Random random = new Random();

    private Integer getRandomInteger(List<Integer> chosenNums) {
        int rand = random.nextInt(mathGameLimitProperties.getMaxValue() + 1
                - mathGameLimitProperties.getMinValue()) + mathGameLimitProperties.getMinValue();
        while (chosenNums.contains(rand)) {
            rand = random.nextInt(mathGameLimitProperties.getMaxValue() + 1
                    - mathGameLimitProperties.getMinValue()) + mathGameLimitProperties.getMinValue();
        }
        return rand;
    }

    private Character getRandomOperator() {
        int rand = random.nextInt(4);
        switch (rand) {
            case 0:
                return '+';
            case 1:
                return '-';
            case 2:
                return '*';
            case 3:
                return '/';
            default:
                throw new QuestionCreationException("Operator for random number: " + rand + " was not found.");
        }
    }

    private Integer performOperation(Character op, Integer a, Integer b) {
        switch (op) {
            case '+':
                return a + b;
            case '-':
                if(a - b > 0) return a - b;
                break;
            case '*':
                return a * b;
            case '/':
                if(a % b == 0) return a / b;
                break;
            default:
                throw new QuestionCreationException("Operator: " + op + " was not found.");
        }
        return null;
    }

    private void saveQuestion(String quest, Integer answer, String solution, Level level) {
        Question question = new Question();
        question.setQuestion(quest);
        question.setAnswer(answer);
        question.setSolution(solution);
        question.setLevel(level);
        questionService.create(question);
    }

    private MathQuestionDTO getNewQuestion(List<Integer> chosenNums, StringBuilder solution) {
        char op = getRandomOperator();
        Integer cur = getRandomInteger(chosenNums), calcRes, answer = cur;
        int calcCounter = 0;
        while ((op == '/' && cur.equals(1)) || (calcRes = performOperation(op, answer, cur)) == null) {
            cur = getRandomInteger(chosenNums);
            calcCounter++;
            if(calcCounter == 10) { // FIXME: hardcoded value for now
                calcCounter = 0;
                op = getRandomOperator();
            }
        }

        chosenNums.add(cur);
        solution.append(",").append(answer).append(op).append(cur).append("=").append(calcRes);
        answer = calcRes;
        return new MathQuestionDTO(answer, solution);
    }

    @Override
    @Transactional
    public ApiResponseDTO generateMathQuestions() {
        Level level = levelService.getByLevel(1); // FIXME: hardcoded level

        for(int i = 0; i < mathGameLimitProperties.getQuestionsNum(); i++) {
            int counter = 0;
            Integer cur = getRandomInteger(Collections.emptyList()), answer = cur;
            StringBuilder solution = new StringBuilder();
            List<Integer> chosenNums = new ArrayList<>(Collections.singletonList(cur));

            while(counter != mathGameLimitProperties.getNumPerQuestion() - 1) {
                MathQuestionDTO newQuestion = getNewQuestion(chosenNums, solution);
                answer = newQuestion.getAnswer();
                solution = newQuestion.getSolution();
                counter++;
            }

            Collections.shuffle(chosenNums);
            String question = chosenNums.toString().replace(", ", ",");
            saveQuestion(question.substring(1, question.length() - 1), answer, solution.substring(1), level);
        }

        return new ApiResponseDTO(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(),
                mathGameLimitProperties.getQuestionsNum() + " questions generated successfully.");
    }
}
