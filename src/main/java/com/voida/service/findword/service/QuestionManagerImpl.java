package com.voida.service.findword.service;

import com.voida.service.findword.config.LimitProperties;
import com.voida.service.findword.persistence.model.*;
import com.voida.service.findword.persistence.model.entity.*;
import com.voida.service.findword.persistence.repository.PointsRepository;
import com.voida.service.findword.service.exception.QuestionLimitExceededException;
import com.voida.service.findword.service.exception.QuestionMismatchException;
import com.voida.service.findword.service.exception.QuestionNotFoundException;
import com.voida.service.findword.web.dto.AnswerResultDTO;
import com.voida.service.findword.web.dto.HintDTO;
import com.voida.service.findword.web.dto.QuestionDTO;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
//@EnableScheduling
public class QuestionManagerImpl implements QuestionManager {
    private final PointsRepository pointsRepository;
    private final UserScoreService userScoreService;
    private final UserService userService;
    private final UserQuestionService userQuestionService;
    private final ModelMapper modelMapper;
    private final UserGameService userGameService;
    private final LimitProperties limitProperties;
    private final QuestionService questionService;

    private QuestionDTO getRandomQuestion(Integer userId, Integer languageId, UserGame userGame) {
        List<Question> questions = questionService.getQuestions(languageId, 1); // FIXME: level 1 hardcoded for now
        List<Integer> userQuestionIds = userQuestionService.getLastDayByUserId(userId)
                .stream()
                .map(userQuestion -> modelMapper.map(userQuestion.getQuestion().getId(), Integer.class))
                .collect(Collectors.toList());
        if(userQuestionIds.size() >= questions.size()) throw new QuestionNotFoundException("There are not new questions" +
                " for user with id: " + userId + "."); // don't repeat questions

        RandomQuestion randomQuestion = questionService.getRandomQuestion(questions, userQuestionIds);
        userQuestionService.create(userGame, randomQuestion.getQuestion());
        userGameService.updatePlayedWords(userGame);
        return new QuestionDTO(randomQuestion.getFormattedQuestion(), randomQuestion.getAnswer(), userGameService.getGameDTO(userGame));
    }

    private UserGame getUserGame(User user) {
        UserGame userGame = userGameService.getCurrentGame(user.getAccountId());
        if(userGame == null) return userGameService.create(user);

        if(userGame.getQuestionNum().equals(userGame.getQuestionPlayed()) ||
                userGame.getExpirationDate().compareTo(Calendar.getInstance().getTime()) < 0)
            throw new QuestionLimitExceededException("You have reached your daily limit: "
                    + userGame.getQuestionNum() + " questions for this game.");
        return userGame;
    }

    @Override
    public QuestionDTO getQuestion(Long accountId, Integer languageId) {
        User user = userService.createIfNotExists(accountId); // save user on first call if not exists
        UserGame userGame = getUserGame(user);
        return getRandomQuestion(user.getId(), languageId, userGame);
    }

    @Override
    public AnswerResultDTO checkAnswer(Long accountId, String userAnswer, Long spentTime) {
        UserQuestion userQuestion = userQuestionService.getLatestByAccountId(accountId);
        if(userQuestion.getAnswer() != null) throw new QuestionMismatchException("You haven't been given new question.");

        AnswerResult answerResult = questionService.checkAnswer(userQuestion, userAnswer.toUpperCase());
        int answerPoints = 0;
        boolean isCorrect = false;
        if(answerResult.getIsValid()) {
            Points points = pointsRepository.findByAnswerDiff(answerResult.getAnswerDiff());
            if(points != null) {
                answerPoints = points.getPoints();
                isCorrect = true;
                userScoreService.updateUserScore(userQuestion.getUserGame().getUser().getId(), answerPoints);
            }
        }

        userQuestionService.setUserAns(userQuestion, userAnswer.toUpperCase(), answerPoints, spentTime);
        userGameService.updateGameScore(userQuestion.getUserGame(), answerPoints, spentTime);
        return new AnswerResultDTO(isCorrect, answerPoints, answerResult.getCorrectAnswer());
    }

    @Override
    public HintDTO getHint(Long accountId) {
        UserGame userGame = userGameService.getCurrentGame(accountId);
        if(userGame == null) throw new QuestionNotFoundException("User with accountId: " + accountId + " has not got questions yet.");
        userGameService.updatePlayedHints(userGame);
        return new HintDTO(userQuestionService.getLatestByAccountId(accountId).getQuestion().getHint(), userGame.getHintsPlayed(),
                limitProperties.getHintsNumPerGame() - userGame.getHintsPlayed());
    }

    // runs every minute and checks last minutes updates
//    @Scheduled(fixedDelay = 60000, initialDelay = 60000)
//    public void updateUnansweredQuestions() {
//        userQuestionService.updateUnansweredQuestions(dateTimeService.getDateMinutesAgo(2),
//                dateTimeService.getDateMinutesAgo(1),
//                limitProperties.getSecondsPerQuestion());
//    }

}
