package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class PageInfoNotFoundException extends RuntimeException {
    public PageInfoNotFoundException(String s) {
        super(s);
    }
}
