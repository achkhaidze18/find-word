package com.voida.service.findword.service;

import com.voida.service.findword.persistence.repository.PeriodRepository;
import com.voida.service.findword.service.exception.PeriodNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PeriodServiceImpl implements PeriodService {
    private final PeriodRepository periodRepository;

    @Override
    @Cacheable("period")
    public void checkPeriodExistence(Integer id) {
        periodRepository.findById(id)
                .orElseThrow(() -> new PeriodNotFoundException("Period with id: " + id + " was not found."));
    }

}
