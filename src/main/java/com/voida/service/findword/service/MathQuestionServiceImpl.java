package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.AnswerResult;
import com.voida.service.findword.persistence.model.entity.Question;
import com.voida.service.findword.persistence.model.RandomQuestion;
import com.voida.service.findword.persistence.model.entity.UserQuestion;
import com.voida.service.findword.service.exception.AnswerEvaluationException;
import com.voida.service.findword.service.exception.QuestionNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
@Profile({"mathGameDev", "mathGameTest", "mathGameProd"})
public class MathQuestionServiceImpl implements QuestionService {
    private final QuestionDAO questionDAO;

    @Override
    public List<Question> getQuestions(Integer languageId, Integer level) {
        List<Question> questions = questionDAO.getAllByLevel(level);
        if(questions.size() == 0) throw new QuestionNotFoundException("No questions in database for level: " + level + ".");
        return questions;
    }

    @Override
    public RandomQuestion getRandomQuestion(List<Question> questions, List<Integer> userQuestionIds) {
        Question question = questions.get(new Random().nextInt(questions.size()));
        while (userQuestionIds.contains(question.getId())) {
            question = questions.get(new Random().nextInt(questions.size()));
        }
        return new RandomQuestion(question, question.getQuestion(), question.getAnswer().toString());
    }

    private Integer evaluateAnswer(String userAnswer) {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        try {
            return (Integer) engine.eval(userAnswer);
        } catch (ScriptException e) {
            throw new AnswerEvaluationException(e.getMessage());
        }
    }

    @Override
    public AnswerResult checkAnswer(UserQuestion userQuestion, String answer) {
        Integer correctAns = userQuestion.getQuestion().getAnswer();
        Integer userAns = evaluateAnswer(answer);
        Integer diff = Math.abs(correctAns - userAns);
        return new AnswerResult(diff, true, userQuestion.getQuestion().getSolution());
    }

}
