package com.voida.service.findword.service;

import com.voida.service.findword.web.dto.ApiResponseDTO;
import org.springframework.context.annotation.Profile;

@Profile({"mathGameDev", "mathGameTest", "mathGameProd"})
public interface QuestionGenerationService {
    ApiResponseDTO generateMathQuestions();
}
