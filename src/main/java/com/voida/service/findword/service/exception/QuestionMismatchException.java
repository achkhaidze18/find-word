package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class QuestionMismatchException extends RuntimeException {
    public QuestionMismatchException(String message) {
        super(message);
    }
}
