package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class QuestionLimitExceededException extends RuntimeException {
    public QuestionLimitExceededException(String s) {
        super(s);
    }
}
