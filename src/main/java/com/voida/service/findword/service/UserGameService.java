package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.User;
import com.voida.service.findword.persistence.model.entity.UserGame;
import com.voida.service.findword.web.dto.GameDTO;
import com.voida.service.findword.web.dto.LeaderScore;

import java.util.Date;
import java.util.List;

public interface UserGameService {
    UserGame getCurrentGame(Long accountId);
    UserGame create(User user);
    void updatePlayedWords(UserGame userGame);
    List<UserGame> getByAccountId(Long accountId);
    List<UserGame> getByAccountIdAndPage(Long accountId, Integer page);
    GameDTO getGameDTO(UserGame userGame);
    void updateGameScore(UserGame userGame, Integer points, Long spentTime);
    void updatePlayedHints(UserGame userGame);
    
    List<LeaderScore> findLeadersByPeriod(Integer userNum, Date startDate);
    LeaderScore getUserPoints(Long accountId, Date startDate);
    Integer getUserPosition(Long accountId, Long points, Date startDate, Long spentTime);
}
