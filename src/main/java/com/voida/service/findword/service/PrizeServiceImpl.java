package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.Prize;
import com.voida.service.findword.persistence.repository.PrizeRepository;
import com.voida.service.findword.web.dto.PrizeDTO;
import com.voida.service.findword.web.dto.PrizesDTO;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PrizeServiceImpl implements PrizeService {
    private final PrizeRepository prizeRepository;
    private final ModelMapper modelMapper;
    private final PeriodService periodService;

    @Override
    public PrizesDTO getAllByPeriodId(Integer periodId) {
        List<Prize> prizes;
        if(periodId == null) prizes = prizeRepository.findAll();
        else {
            periodService.checkPeriodExistence(periodId); // make sure period exists or throw exception
            prizes = prizeRepository.findByPeriodId(periodId);
        }
        return new PrizesDTO(prizes
                .stream()
                .map(prize -> modelMapper.map(prize, PrizeDTO.class))
                .collect(Collectors.toList()));
    }

}
