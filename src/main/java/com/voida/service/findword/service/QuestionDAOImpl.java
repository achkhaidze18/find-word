package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.Question;
import com.voida.service.findword.persistence.repository.QuestionRepository;
import com.voida.service.findword.service.exception.QuestionCreationException;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class QuestionDAOImpl implements QuestionDAO {
    private final QuestionRepository questionRepository;

    @Override
    public List<Question> getAllByLanguageIdAndLevel(Integer languageId, Integer level) {
        return questionRepository.findAllByLevelAndLanguageId(level, languageId);
    }

    @Override
    @Cacheable("questions")
    public List<Question> getAllByLevel(Integer level) {
        return questionRepository.findAllByLevel(level);
    }

    @Override
    public Boolean questionExists(String question) {
        List<Question> found = questionRepository.findByQuestion(question);
        return found.size() != 0;
    }

    @Override
    public void create(Question question) {
        try {
            questionRepository.save(question);
        } catch (Throwable t) {
            throw new QuestionCreationException(t.getMessage());
        }
    }

}
