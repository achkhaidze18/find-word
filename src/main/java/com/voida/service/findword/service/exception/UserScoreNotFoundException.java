package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class UserScoreNotFoundException extends RuntimeException {
    public UserScoreNotFoundException(String message) {
        super(message);
    }
}
