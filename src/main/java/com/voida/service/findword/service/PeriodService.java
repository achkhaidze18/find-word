package com.voida.service.findword.service;

public interface PeriodService {
    void checkPeriodExistence(Integer id);
}
