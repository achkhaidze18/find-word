package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@NoStackTrace
@Getter
@Setter
@AllArgsConstructor
public class CustomException extends RuntimeException {
    private Integer code;
    private String message;
}