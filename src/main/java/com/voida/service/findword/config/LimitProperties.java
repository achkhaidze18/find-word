package com.voida.service.findword.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "limits")
@Setter
@Getter
public class LimitProperties {
    private Integer questionsNumPerGame;
    private Integer hintsNumPerGame;
    private Integer leaderboardUserNum;
    private Long secondsPerQuestion;
}
