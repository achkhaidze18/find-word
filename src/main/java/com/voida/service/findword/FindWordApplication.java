package com.voida.service.findword;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.voida")
public class FindWordApplication {

    public static void main(String[] args) {
        SpringApplication.run(FindWordApplication.class, args);
    }
}

// comments for me, delete later
// java -Dspring.config.location=C:\Users\Anamaria\Desktop\findWordJarTest\config\application-test.properties -Dlogging.config=C:\Users\Anamaria\Desktop\findWordJarTest\config\log4j2-spring.xml -jar find-word-1.0.0.jar
// nohup java -Dspring.config.location=./conf/application-test.properties -Dlogging.config=./conf/log4j2-spring.xml -jar find-word-1.0.0.jar